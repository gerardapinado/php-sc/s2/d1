<?php

/*
    [SECTION] Repetition Control Structure
*/
// while loop
function whileLoop(){
    $count = 5;
    while($count !== 0){
        echo $count . '</br>';
        $count--;
    }
}

// do-while loop
function doWhileLoop(){
    $count = 5;

    do{
        echo $count . '</br>';
        $count--;
    }while($count > 0);
}

// for loop
function forLoop(){
    for($count=0;$count<=5;$count++){
        echo $count . '</br>';
    }
}

/* 
    [SECTION] Array Manipulation
*/
$studentNumbers = array('1923', '1924', '1925', '2916');
$studentNumbers = ['1923', '1924', '1925', '2916'];
// simple array
$grades = [98.5, 88.4, 99.99];
// associative array
$gradePeroids = ['firstGrading'=> 98.5, 'secondGrading'=>94.3, 'thridGrading'=>89.2, 'fourthGrading'=>90.1];
// multi-dimensional array
$heroes = [
    ['ironman', 'hulk', 'thor'],
    ['wolvarine', 'storm', 'cyclops'],
    ['batman', 'superman', 'aquaman']
];
// associative multi-dimensional array
$powers = [
    'signature' => ['sig1', 'sig2'],
    'power' => ['pow1', 'pow2']
];
$computerBrands = ['acer', 'asus', 'lenovo', 'dell'];