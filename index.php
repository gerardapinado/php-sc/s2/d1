<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>s2: Discussion</title>
</head>
<body>
    <h1>Repetition Control Structures</h1>
    
    <?php 
        echo '<h2>While Loop</h2>';
        whileLoop(); 
        echo '<h2>Do-While Loop</h2>';
        doWhileLoop();
        echo '<h2>For Loop</h2>';
        forLoop();
        echo '<h2>Array Manipulation</h2>';
    ?>
    <ul>
        <h3>foreach</h3>
        <?php 
            foreach($grades as $grade){
                echo "<li> $grade </li>";
            } 
        ?>
    </ul>
    <?php
        echo '<h2>Associative Array</h2>'; 
        echo $gradePeroids['firstGrading'];
        foreach($gradePeroids as $period){
            echo "<li> $period </li>";
        } 
    ?>
    <ul>
    <?php foreach($gradePeroids as $period => $grade) { ?>
			<li>Grade in <?= $period //<?=is the shortcut for <?php echo ?> is <?= $grade ?></li>
	<?php } ?>
    </ul>
    <h3>Multi-dimensional Array</h3>
    <ul>
        <?php 
            foreach($heroes as $team){
                foreach($team as $members){
                    echo "<li> $members </li>";  
                }
            }
        ?>
    </ul>
    <ul>
    <?php 
            foreach($powers as $label => $powerGroup){
                foreach($powerGroup as $power){
                    echo "<li> $label: $power </li>";  
                }
            }
        ?>
    </ul>
    <h3>Array Methods</h3>
    <?php 
    array_push($computerBrands, 'apple');
    print_r($computerBrands);
    echo "<br/>";
    array_unshift($computerBrands, 'dell');
    print_r($computerBrands);
    echo "<br/>";
    array_pop($computerBrands);
    print_r($computerBrands);
    echo "<br/>";
    array_shift($computerBrands);
    print_r($computerBrands);
    ?>
</body>
</html>